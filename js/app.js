// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', [
    'ionic',
    'ngSanitize',
    'angular.filter',
    'starter.controllers',
    'ionic.utils',
    'ionic-ratings',
	'ngCordova'
])
    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }


        });
    })
    .run(function ($localstorage, $rootScope, RequestService) {

        if( $localstorage.getObject('city') ) {

        } else {
            $localstorage.setObject('city',{id:2, name: "Алматы"});
        }
        var params = {};

        RequestService.get('cities.data', params).then(function (result) {

            $rootScope.cities = result.data;
            console.log($localstorage.getObject('city'));
        });


    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.main', {
                url: '/main',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/main.html',
                        controller: 'MainCtrl'
                    }
                },

            })

            .state('app.cinemas', {
                url: '/cinemas',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/cinemas.html',
                        controller: 'CinemasCtrl'
                    }
                }
            })
			.state('app.smsform', {
                url: '/smsform/:time/:movie/:cinema/:child/:stud/:adult',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/smsform.html',
                        controller: 'SmsCtrl'
                    }
                }
            })
            .state('app.top', {
                url: '/top',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/top.html',
                        controller: 'TopCtrl'
                    }
                }
            })
            .state('app.premiere', {
                url: '/premiere',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/premiere.html',
                        controller: 'PremiereCtrl'
                    }
                }
            })
            .state('app.movies', {
                url: '/movies/:day',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/movies.html',
                        controller: 'MoviesCtrl'
                    }
                }
            })
            .state('app.movie-schedule', {
                url: '/movie-schedule/:movieID/:day',
                params: {
                    day: 0,
                    filmData: null
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/movie-schedule.html',
                        controller: 'MovieScheduleCtrl'
                    }
                }
            })
            .state('app.cinema-schedule', {
                url: '/cinema-schedule/:cinemaID/:day',
                params: {
                    day: 0,
                    cinemaData: null
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/cinema-schedule.html',
                        controller: 'CinemaScheduleCtrl'
                    }
                }
            })
            .state('app.cinema', {
                url: '/cinema/:cinemaId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/cinema.html',
                        controller: 'CinemaFullCtrl'
                    }
                },
                params: {
                    data: null,
                }

            })
            .state('app.movie', {
                url: '/movie/:movieId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/movie.html',
                        controller: 'MovieFullCtrl'
                    }
                },
                params: {
                    data: null,
                }

            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/main');
    })
    .filter('onlyTime', function () {

        // In the return function, we must pass in a single parameter which will be the data we will work on.
        // We have the ability to support multiple other parameters that can be passed into the filter optionally
        return function (input) {
            var array = input.split(' ');

            var output = array[1];

            // Do filter work here

            return output;

        }

    })

    .filter('getById', function () {
        return function (input, id) {
            var i = 0, len = input.length;
            for (; i < len; i++) {
                if (+input[i].id == +id) {
                    return input[i];
                }
            }
            return null;
        }
    });
