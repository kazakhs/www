angular.module('starter.controllers')
  .controller('MovieScheduleCtrl', function ($scope, $rootScope, $state, $stateParams, $localstorage, $timeout, $filter, $ionicPopover, RequestService) {
    //console.log('FilmDATA', $stateParams.filmData)
  $scope.daytext="сегодня";
    $scope.Film = $stateParams.filmData;
    var cityId = $localstorage.getObject('city').id;
    var cinemaParams = {
      city: cityId
    };
    var params = {
      movie: $stateParams.movieID,
      day: $stateParams.day,
      sort: 0,
      city: $localstorage.getObject('city')['id']

    };

    $scope.getCinemaData = function (cinemaId, field) {

      //console.log('cinemaID:', cinemaId)
      var thisCinema = $filter('getById')($scope.cinemas, cinemaId);
      if (typeof field != 'undefined') {
        return thisCinema[field];
      } else {
        return thisCinema;
        //console.log('with ID',cinemaId )
        // console.log('with INDEX',idx )
      }


    };
 
 $scope.share=function(moviename,time,cinema,child,student,adult){
$state.go('app.smsform',{time:time,movie:moviename,cinema:cinema,child:child,stud:student,adult:adult});

 };

    $scope.showSessionsforDay = function (dayId) {
      console.log('show another session');

         var params = {
      movie: $stateParams.movieID,
      day: dayId,
      sort: 0,
      city: $localstorage.getObject('city')['id']

    };

          if(dayId==0)
      $scope.daytext="сегодня";
      else if(dayId==1)
      {
      $scope.daytext="завтра";  
      }
      else if(dayId==2)
      {
      $scope.daytext="послезавтра"; 
      }

    RequestService.get('cinemas.data', cinemaParams).then(function (result) {

      $scope.cinemas = result.data;

      RequestService.get('schedule_movie.data', params).then(function (result) {

        $scope.FilmSessions = result.data.times;
        console.log('FilmSessions', $scope.FilmSessions);
        console.log('FilmSessions Length', $scope.FilmSessions.length);
        $scope.TodayDate = result.data.date;
      });

      console.log("CINEMAS: ", $scope.cinemas);
    });

    };
    function setTitle(){
      var expr = $stateParams.day;
      return $timeout(function () {
       if( expr ==0  ){
         $scope.pagetitle ='Расписание на сегодня'
       } else if( expr ==1 ) {
         $scope.pagetitle ='Расписание на завтра'
       } else {
         $scope.pagetitle ='Послезавтра'
       }

      }, 500);

    };

  setTitle();



    RequestService.get('cinemas.data', cinemaParams).then(function (result) {

      $scope.cinemas = result.data;

      RequestService.get('schedule_movie.data', params).then(function (result) {

        $scope.FilmSessions = result.data.times;
        console.log('FilmSessions', $scope.FilmSessions);
        console.log('FilmSessions Length', $scope.FilmSessions.length);
        $scope.TodayDate = result.data.date;
      });

      console.log("CINEMAS: ", $scope.cinemas);
    });


    console.log('MovieScheduleCtrl started');


    // .fromTemplate() method

    $ionicPopover.fromTemplateUrl('templates/popover.html', {
      scope: $scope,
    }).then(function (popover) {
      $scope.popover = popover;
    });


    $scope.openPopover = function ($event) {
      $scope.popover.show($event);
    };
    $scope.closePopover = function () {
      $scope.popover.hide();
    };
    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.popover.remove();
    });
    // Execute action on hide popover
    $scope.$on('popover.hidden', function () {
      // Execute action
    });
    // Execute action on remove popover
    $scope.$on('popover.removed', function () {
      // Execute action
    });

  })
