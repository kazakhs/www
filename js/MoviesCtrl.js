angular.module('starter.controllers')
    .controller('MoviesCtrl', function ($scope, $stateParams, $http, $timeout, RequestService, $localstorage) {
        var cityId = $localstorage.getObject('city').id;
        var day = $stateParams.day;
        var params = {
            city: cityId,
            day: day

        };
        RequestService.get('movies.data', params).then(function(result){

            $scope.movies = result.data;

            //console.log($scope.premieres);
        });


})
