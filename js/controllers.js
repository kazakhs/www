angular.module('starter.controllers', [])

  .controller('AppCtrl', function ($scope, $rootScope, $state, $localstorage, $timeout) {






    $scope.myCity = {
      data: {
        id: 2,
        name: "Алматы"
      }

    };
    if ($localstorage.getObject('city')) {
      $scope.myCity.data = $localstorage.getObject('city');
    }
    $scope.cityFunc = {
      get: function () {
        var data = $localstorage.getObject('city');
        console.log('getting city:');
        console.log(data);
        return data;
      },
      set: function () {
        $state.go($state.current, {}, {reload: true});
        console.log('cityID: ', $scope.myCity.data);
        $localstorage.setObject('city', $scope.myCity.data);
      }

    };

  })
    .controller('ScheduleCtrl', function ($scope) {

    })

  .controller('MainCtrl', function ($scope, $localstorage, $filter, $stateParams, RequestService) {
    var premiereParams, scheduleParams;
    var cityId = $localstorage.getObject('city').id;

    $scope.getCinemaName = function( cinemaId ) {
      //console.log('cinemaName func ')
      var thisCinema = $filter('getById')($scope.cinemas, cinemaId)
      var name = thisCinema.name;
      return name
    };


    // Get premiere film list

    premiereParams = {
      city: cityId,
      type: 2

    };
    RequestService.get('premiere.data', premiereParams).then(function (result) {

      $scope.premieres = result.data;

      //console.log($scope.premieres);
    });


    // Get films shedule for today ======>


    scheduleParams = {
      count: 20,
      city: cityId
    };

    RequestService.get('schedule_city.data', scheduleParams).then(function (result) {

      $scope.schedule = result.data.times;

      console.log("Shedule data", $scope.schedule);
    });


    // Get cinemas list in current city ===>

    var cinemaParams = {
      city: cityId
    };
    RequestService.get('cinemas.data', cinemaParams).then(function (result) {

      $scope.cinemas = result.data;
      console.log("CINEMAS: ", $scope.cinemas);
    });



  });


